export interface IResponse<T> {
  count: number;
  next: null;
  previous: null;
  results: T;
}

export interface IOffer {
  id?: number;
  material: string;
  date?: string;
  buyPrice: number;
  sellPrice: number;
  marge?: number;
  margePercent?: number;
}

export interface IDictionary {
  id: number;
  label: string;
}

export interface INewEntry {
  label: string;
}

export interface IMaterial extends IDictionary{
  date?: string;
  bestCraftPrice: string;
  offers: IOffer[];
}
