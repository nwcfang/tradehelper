const HOST = process.env.REACT_APP_HOST || 'localhost';
const PORT = process.env.REACT_APP_PORT || '8000';
const HTTP = process.env.REACT_APP_HTTP || 'http';
const ROOT_URI = process.env.REACT_APP_ROOT_URI || '';
const BASE_URL = `${HTTP}://${HOST}:${PORT}`;

export const API = {
  BASE: () => `${BASE_URL}${ROOT_URI}`,
  MATERIAL: {
    GET_MATERIAL: () =>
      `${API.BASE()}/materials.json`,
    GET_NEED_UPDATE: () =>
      `${API.BASE()}/need_to_update.json`,
    POST_MATERIAL: () =>
      `${API.BASE()}/materials/`
  },
  USER: {
    LOGIN: () =>
      `${API.BASE()}/rest-auth/login/`,
    LOGOUT: () =>
      `${API.BASE()}/rest-auth/logout/`,
  },
  OFFER: {
    GET_OFFERS_BY_MATERIAL : (params: {materialId: number}) =>
      `${API.BASE()}/offers.json/?materialId=${params.materialId}`,
    GET_TOP_OFFERS: () =>
      `${API.BASE()}/top.json`,
    CREATE_OFFER: () =>
      `${API.BASE()}/offers/`,
  },
};