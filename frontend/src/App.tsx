import * as React from 'react';
import { Route, NavLink } from "react-router-dom";
import { Layout, Menu, Breadcrumb } from 'antd';

import { Offers, AuthForm, User, Top, NeedUpdate, Admin } from './components';

import { IOfferService, OfferService as OfferServiceClass } from './services/OfferService';
import { IMaterialService, MaterialService as MaterialServiceClass } from './services/MaterialService';

import 'antd/dist/antd.css';
import './App.scss';
import { Distribution } from './components/Distribution';

interface IState {
  userName: string | null;
  activeNavLink: number;
}

class App extends React.Component<any, IState> {

  public static OfferService: IOfferService = new OfferServiceClass();
  public static MaterialService: IMaterialService = new MaterialServiceClass();

  public state = {
    userName: localStorage.getItem('userName'),
    activeNavLink: 1,
  };

  public render() {
    const { userName, activeNavLink } = this.state;
    const { Header, Content, Footer } = Layout;

    return (
      <div className="App">
        <Layout className="layout">
          <Header>
            <div className="logo" />
            <Menu
              theme="dark"
              mode="horizontal"
              selectedKeys={[activeNavLink.toString()]}
              style={{ lineHeight: '64px' }}
            >
              <Menu.Item onClick={this.setActiveNavLink(4)} key="4">
                <NavLink to={'/top'}>Top</NavLink>
              </Menu.Item>
              <Menu.Item onClick={this.setActiveNavLink(1)} key="1">
                <NavLink to={'/'}>Offers</NavLink>
              </Menu.Item>
              <Menu.Item onClick={this.setActiveNavLink(6)} key="6">
                <NavLink to={'/needUpdate'}>Need to Update</NavLink>
              </Menu.Item>
              <Menu.Item onClick={this.setActiveNavLink(7)} key="7">
                <NavLink to={'/admin'}>Admin</NavLink>
              </Menu.Item>
              <Menu.Item onClick={this.setActiveNavLink(5)} key="5">
                <NavLink to={'/distribution'}>Distribution</NavLink>
              </Menu.Item>
              {userName ? (
                <Menu.Item onClick={this.setActiveNavLink(2)} key="2">
                  <NavLink to={'/user'}>{userName}</NavLink>
                </Menu.Item>
              ) : (
                <Menu.Item onClick={this.setActiveNavLink(3)} key="3">
                  <NavLink to={`/login`}>Войти</NavLink>
                </Menu.Item>
              )}
            </Menu>
          </Header>
          <Content className="app-content">
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>List</Breadcrumb.Item>
              <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb>
            <div className="content">
              <Route path="/" exact={true} component={Offers} />
              <Route path="/login" render={this.renderLoginForm} />
              <Route path="/user" render={this.renderUserForm}/>
              <Route path="/top" render={this.renderTop}/>
              <Route path="/distribution" render={this.renderDistribution}/>
              <Route path="/needUpdate" render={this.renderNeedUpdate}/>
              <Route path="/admin" render={this.renderAdmin}/>
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            Tradehelper ©2018 Created by Mikhail Gusev
          </Footer>
        </Layout>
      </div>
    );
  }

  private renderDistribution = () => <Distribution/>;

  private renderAdmin = (props: any) => <Admin {...props} MaterialService={App.MaterialService}/>;

  private renderNeedUpdate = (props: any) => <NeedUpdate
    {...props}
    MaterialService={App.MaterialService}
    OfferService={App.OfferService}
  />;

  private renderTop = (props: any) => <Top {...props} OfferService={App.OfferService} />;

  private renderUserForm = (props: any) => <User {...props} updateUserName={this.updateUserName} />;

  private renderLoginForm = (props: any) => <AuthForm {...props} updateUserName={this.updateUserName} />;

  private updateUserName = () => {
    this.setState({userName: localStorage.getItem('userName')});
    this.resetActiveNavLink();
  };

  private resetActiveNavLink = () => {
    this.setState({ activeNavLink: 1 })
  };

  private setActiveNavLink = (key: number) => () => {
    this.setState({ activeNavLink: key })
  }
}

export default App;
