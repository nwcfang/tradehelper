import { STORAGE_PREFIX, TOKEN_KEY } from '../constants/common';

function getCookie(name: string) {
  if (document.cookie && document.cookie !== '') {
    const cookies = document.cookie.split(';');
    for (const cookie of cookies) {
      const cookiePair = cookie.split('=');
      if (cookiePair[0] === name) {
        return cookiePair[1]
      }
    }
  }
  return null;
}

class HttpDataSource {
  public get(url: string) {
    return this.send(encodeURI(url), 'GET');
  }

  public post(url: string, data = {}) {
    return this.send(encodeURI(url), 'POST', data);
  }

  public put(url: string, data = {}) {
    return this.send(encodeURI(url), 'PUT', data);
  }

  public remove(url: string, data = {}) {
    return this.send(encodeURI(url), 'DELETE', data);
  }

  private send(url: string, method = 'POST', data?: any): Promise<any> {
    const headers: {[k: string]: any} = {
      'Content-Type': 'application/json;charset=UTF-8',
      'X-CSRFToken': getCookie('csrftoken'),
    };
    const token = localStorage.getItem('token');

    if (token) {
      headers.Authorization = `Token ${token}`;
    }

    return fetch(url, {
      body: data && JSON.stringify(data),
      headers,
      method,
    }).then(this.response);
  }

  private response = async (response: any): Promise<any> => {
    const parsed: any = await response.json();
    if (response.status === 401) {
      window.localStorage.removeItem(`${STORAGE_PREFIX}_${TOKEN_KEY}`);
    }

    return response.ok ? Promise.resolve(parsed) : Promise.reject(parsed);
  }
}

export { HttpDataSource };
