import { API } from '../constants/api';
import { HttpDataSource } from '../sources/HttpDataSource';

interface IUser {
  key: string;
}

interface IUserService {
  login(username: string, password: string): Promise<IUser>;
  logout(): any;
}

class UserService implements IUserService{

  protected http: HttpDataSource;

  constructor() {
    this.http = new HttpDataSource();
  }

  public login(username: string, password: string): Promise<IUser> {
    return this.http.post(API.USER.LOGIN(), {username, email: '', password})
  }

  public logout(): any {
    return this.http.post(API.USER.LOGOUT(), {})
  }

}

export { UserService, IUserService, IUser };
