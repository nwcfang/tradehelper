import { API } from '../constants/api';
import { HttpDataSource } from '../sources/HttpDataSource';
import { IMaterial, INewEntry, IResponse } from '../types/DTO';


interface IMaterialService {
  getMaterials(): Promise<IResponse<IMaterial[]>>;
  getNeedUpdate(): Promise<IResponse<IMaterial[]>>;
  createMaterial(data: INewEntry): Promise<IResponse<IMaterial>>;
}

class MaterialService  implements IMaterialService{

  protected http: HttpDataSource;

  constructor() {

    this.http = new HttpDataSource();
  }

  public getMaterials(): Promise<IResponse<IMaterial[]>> {
    return this.http.get(API.MATERIAL.GET_MATERIAL())
  }

  public getNeedUpdate(): Promise<IResponse<IMaterial[]>> {
    return this.http.get(API.MATERIAL.GET_NEED_UPDATE())
  }

  public createMaterial(data: INewEntry) {
    return this.http.post(API.MATERIAL.POST_MATERIAL(), data)
  }
}

export { MaterialService, IMaterialService };