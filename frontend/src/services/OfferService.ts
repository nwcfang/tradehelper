import { API } from '../constants/api';
import { HttpDataSource } from '../sources/HttpDataSource';

import { IResponse, IOffer } from '../types/DTO';

interface IOfferService {
  createOffer(data: IOffer): Promise<IOffer>;
  getOffersByMaterial(materialId: number): Promise<IOffer[]>;
  getTopOffers(): Promise<IResponse<IOffer[]>>;
}

class OfferService implements IOfferService{

  protected http: HttpDataSource;

  constructor() {

    this.http = new HttpDataSource();
  }

  public getOffersByMaterial(materialId: number): Promise<IOffer[]> {
    return this.http.get(API.OFFER.GET_OFFERS_BY_MATERIAL({ materialId }))
  }

  public createOffer(data: IOffer): Promise<IOffer> {
    return this.http.post(API.OFFER.CREATE_OFFER(), data)
  }

  public getTopOffers(): Promise<IResponse<IOffer[]>> {
    return this.http.get(API.OFFER.GET_TOP_OFFERS())
  }
}

export { OfferService, IOfferService };