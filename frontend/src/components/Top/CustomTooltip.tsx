import * as React from 'react';
import { Card, Icon } from 'antd';
import { TooltipProps } from 'recharts';

import "./Top.scss"


const CustomTooltip = (props: TooltipProps) => {
  const { active, payload } = props;
  if (active && payload) {
    return (
      <Card
        style={{
          textAlign: "left",
        }}
      >
        <p><Icon type="dollar" className="margeColor" /> Прибыль: {payload[0].payload.margePercent}</p>
        <p><Icon type="download" className="buyColor" /> Покупка: {payload[0].payload.buyPrice}</p>
        <p><Icon type="upload" className="sellColor" /> Продажа: {payload[0].payload.sellPrice}</p>
      </Card>
    )
  }
  return null;
};

export { CustomTooltip };