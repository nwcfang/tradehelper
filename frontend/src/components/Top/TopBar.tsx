import * as React from 'react';
import { Bar, BarChart, CartesianGrid, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';

import { CustomTooltip } from './CustomTooltip';

import { IOffer } from '../../types/DTO';


interface IProps {
  data: IOffer[]
}

const tickFormatter = (value: any) => (value + '%');

class TopBar extends React.PureComponent<IProps> {

  public render() {
    const sortedData = this.sortByMarge();
    return (
      <ResponsiveContainer width="100%" height="100%">
        <BarChart data={sortedData}>
          <CartesianGrid strokeDasharray="3 3"/>
          <XAxis dataKey="label" interval={0} height={200} tick={this.CustomizedAxisTick} />
          <YAxis tickFormatter={tickFormatter} />
          <Tooltip itemStyle={{border: "none"}} content={<CustomTooltip />}/>
          <Bar name="Прибыль" dataKey="margePercent" stackId="a" stroke="#52A679" fill="#82ca9d" />
        </BarChart>
      </ResponsiveContainer>
    )
  }

  private sortByMarge = () => {
    const { data } = this.props;
    return data.sort((a, b) => {
      return (b.margePercent || 0) - (a.margePercent || 0);
    })
  };

  private CustomizedAxisTick = (props: any) => {
    const {x, y, payload} = props;

    return (
      <g transform={`translate(${x},${y})`}>
        <text x={0} y={0} dy={16} textAnchor="end" fill="#666" transform="rotate(-90)">{payload.value}</text>
      </g>
    );
  }
}

export { TopBar };