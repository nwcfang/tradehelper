import * as React from 'react';
import { TopBar } from './TopBar';

import { IOfferService } from '../../services/OfferService';
import { IOffer } from '../../types/DTO';

import './Top.scss';

interface IProps {
  OfferService: IOfferService
}

interface IState {
  offers: IOffer[]
}

class Top extends React.Component<IProps, IState> {

  public state = {
    offers: [],
  };

  public componentDidMount() {
    const { OfferService } = this.props;
    OfferService.getTopOffers().then((json) => {
      this.setState({offers: json.results});
    })
  }

  public render () {
    const { offers } = this.state;

    return (
      <div className="Top">
        <TopBar data={offers}/>
      </div>
    );
  }
}

export { Top };