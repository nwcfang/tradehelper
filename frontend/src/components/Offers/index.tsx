import * as React from 'react';
import { Select, InputNumber, Form, Button, Row, Col } from 'antd';

import { SimplePlot } from './SimplePlot';

import { IMaterialService, MaterialService } from '../../services/MaterialService';

import { IMaterial, IOffer } from '../../types/DTO';

import './Offers.scss';
import { IOfferService, OfferService as OfferServiceClass } from '../../services/OfferService';

const Option = Select.Option;
const FormItem = Form.Item;

interface IState {
  MaterialService: IMaterialService;
  OfferService: IOfferService;
  materials: IMaterial[];
  selectedMaterial: IMaterial | undefined;
  newBuyPrice: number;
  newSellPrice: number;
}


class Offers extends React.Component<{}, IState> {

  public state: IState = {
    MaterialService: new MaterialService(),
    OfferService: new OfferServiceClass(),
    materials: [],
    newBuyPrice: 0,
    newSellPrice: 0,
    selectedMaterial: undefined,
  };

  public componentDidMount() {
    this.state.MaterialService.getMaterials()
      .then((json) => {
        const materials = json.results;
        this.setState({
          materials,
        })
      })
  };


  public render() {
    const { selectedMaterial, newBuyPrice, newSellPrice } = this.state;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };
    return (
      <div className="offers">
        <div className="container">
          <SimplePlot material={selectedMaterial}/>
        </div>
        <Row gutter={16} className="controls-box">
          <Col span={24} xs={12}>
            <Form className="controls">
              <FormItem {...formItemLayout} label="Ресурс">
                <Select
                  size="large"
                  showSearch={true}
                  style={{ width: 200 }}
                  placeholder="Выбирите ресурс"
                  optionFilterProp="children"
                  onChange={this.handleMaterialChange}
                  filterOption={this.getFilterOption}
                >
                  {this.renderOptions()}
                </Select>
              </FormItem>
              <FormItem label="Покупкa" {...formItemLayout}>
                <InputNumber
                  size="large"
                  min={0}
                  value={newBuyPrice}
                  onFocus={this.handleFocus}
                  onChange={this.handleBuyChange}
                />
              </FormItem>
              <FormItem label="Продажа" {...formItemLayout} >
                <InputNumber
                  size="large"
                  min={0}
                  value={newSellPrice}
                  onFocus={this.handleFocus}
                  onChange={this.handleSellChange}
                />
              </FormItem>
              <FormItem {...tailFormItemLayout}>
                <Button size="large" onClick={this.handleCreateOffer}>Сохранить</Button>
              </FormItem>
            </Form>
          </Col>
          <Col span={24} xs={12}>
            <p>Лучшая цена с учетом крафта:</p>
            <p>{selectedMaterial && selectedMaterial.bestCraftPrice}</p>
          </Col>
        </Row>
      </div>
    );
  }

  private resetPrices = () => {
    this.setState({
      newBuyPrice: 0,
      newSellPrice: 0
    })
  };

  private handleFocus = (event: React.FormEvent<HTMLInputElement>) => {
    event.currentTarget.select();
  };

  private handleCreateOffer = () => {
    const { newSellPrice, newBuyPrice, selectedMaterial, OfferService } = this.state;
    if (!selectedMaterial) {
      return;
    }

    const data: IOffer = {
      buyPrice: newBuyPrice,
      sellPrice: newSellPrice,
      material: selectedMaterial.label,
    };

    OfferService.createOffer(data)
      .then(()=> {
        this.state.MaterialService.getMaterials()
          .then((json) => {
            const materials = json.results;
            this.setState({
              materials,
            }, () => {
              if (this.state.selectedMaterial) {
                this.handleMaterialChange(this.state.selectedMaterial.id)
              }
            })
          });
      })
      .catch(() => console.error('Not created'));
    this.resetPrices();
  };

  private handleBuyChange = (value: number) => {
    this.setState({
      newBuyPrice: value
    })
  };

  private handleSellChange = (value: number) => {
    this.setState({
      newSellPrice: value
    })
  };

  private renderOptions = (): React.ReactNode => {
    const { materials } = this.state;
    return materials.map((material: IMaterial) =>
      <Option key={material.id.toString()} value={material.id}>{material.label}</Option>
    )
  };

  private getFilterOption = (input: any, option: any) => {
    return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
  };

  private handleMaterialChange = (id: number) => {
    const { materials } = this.state;
    const selectedMaterial: IMaterial | undefined = materials.find((material: IMaterial) => material.id === id);

    this.setState({
      selectedMaterial: selectedMaterial && {...selectedMaterial}
    })
  };
}

export { Offers };