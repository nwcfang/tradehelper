import * as React from 'react';
import { get } from 'lodash';
import { IOffer, IMaterial } from '../../types/DTO';
import { Area, AreaChart, CartesianGrid, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';

interface IProps {
  material?: IMaterial;
}

const tooltipWrapperStyle = {
  textAlign: 'left'
};


class SimplePlot extends React.Component<IProps, {}> {

  public customizedAxisTick = (props: any) =>
  {
    const {x, y, payload} = props;

    return (
      <g transform={`translate(${x},${y})`}>
        <text x={0} y={0} dy={16} textAnchor="end" fill="#666" transform="rotate(-90)">{payload.value}</text>
      </g>
    );
  };

  public render() {
    const { material } = this.props;
    const data: IOffer[] = get(material, 'offers', []);

    return (
      <ResponsiveContainer height="100%">
        <AreaChart data={data}>
          <defs>
            <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
              <stop offset="5%" stopColor="Plum" stopOpacity={0.6}/>
              <stop offset="95%" stopColor="Plum" stopOpacity={0.5}/>
            </linearGradient>
            <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
              <stop offset="95%" stopColor="Yellow" stopOpacity={0.5}/>
            </linearGradient>
          </defs>
          <XAxis dataKey="date" height={180} interval={0} padding={{ left: 0, right: 20 }} tick={this.customizedAxisTick}/>
          <YAxis />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip wrapperStyle={tooltipWrapperStyle}/>
          <Area type="monotone" name="Продажа" dataKey="sellPrice" stroke="DarkOrchid" fillOpacity={1} fill="url(#colorPv)" />
          <Area type="monotone" name="Покупка" dataKey="buyPrice" stroke="DarkOrange" fillOpacity={1} fill="url(#colorUv)" />
        </AreaChart>
      </ResponsiveContainer>

    );
  }
}

export { SimplePlot };
