import * as React from 'react';
import { IMaterialService } from '../../services/MaterialService';

import './Admin.scss';
import { Button, Form, Input, notification } from 'antd';
import { INewEntry } from '../../types/DTO';

interface IProps {
  MaterialService: IMaterialService
}

interface IState {
  newMaterialLabel: string;
}

class Admin extends React.Component<IProps, IState> {

  public state = {
    newMaterialLabel: ''
  };

  public render() {
    const { newMaterialLabel } = this.state;
    return (
      <div className="Admin">
        <Form layout="inline">
          <Form.Item
            label="Добавить материал"
          >
            <Input id="newMaterial" value={newMaterialLabel} onChange={this.handleMaterialChange} />
          </Form.Item>
          <Form.Item>
            <Button onClick={this.handleMaterialSave}>Создать</Button>
          </Form.Item>
        </Form>
      </div>
    );
  }

  private handleMaterialChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
      this.setState({
        newMaterialLabel: value
      })
  };

  private handleMaterialSave = () => {
    const { MaterialService } = this.props;
    const { newMaterialLabel } = this.state;
    const data: INewEntry = {
      label: newMaterialLabel
    };

    MaterialService.createMaterial(data)
      .then((response) => {
        notification['info']({
          message: "Материал добавлен"
        });
        this.setState({
          newMaterialLabel: ''
        });
    })
      .catch(() => notification['error']({
      message: "Не удаллось добавить материал",
      description: "Возможно сервер не отвечает."
    }));
  }
}

export { Admin };