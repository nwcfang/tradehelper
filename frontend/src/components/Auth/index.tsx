import * as React from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import './Auth.scss';
import { IUserService, IUser, UserService as UserServiceClass } from '../../services/UserService';

interface IProps extends FormComponentProps {
  userName: string;
  password: string;
  updateUserName: () => void;
  history: any;
}

interface IState {
  UserService: IUserService;
}

const FormItem = Form.Item;

class Auth extends React.Component<IProps, IState> {

  public state = {
    UserService: new UserServiceClass(),
  };

  public handleSubmit = (e: React.MouseEvent<HTMLElement>) => {
    const { UserService } = this.state;
    const { updateUserName, history } = this.props;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        UserService.login(values.userName, values.password).then((response: IUser) => {
          localStorage.setItem('token', response.key);
          localStorage.setItem('userName', values.userName);
          history.push('/');
          updateUserName();
        });
      }
    });
  };

  public render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="Auth">
        <Form onSubmit={this.handleSubmit} className="login-form">
          <FormItem>
            {getFieldDecorator('userName', {
              rules: [{ required: true, message: 'Пожалуйста введите имя пользователя!' }],
            })(
              <Input size="large" prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Имя пользователя" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Пожалуйста введите пароль!' }],
            })(
              <Input size="large" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Пароль" />
            )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: true,
            })(
              <Checkbox>Запомнить</Checkbox>
            )}
            <a className="login-form-forgot" href="">Забыл пароль</a>
            <Button size="large" type="primary" htmlType="submit" className="login-form-button">
              Log in
            </Button>
            Or <a href="">register now!</a>
          </FormItem>
        </Form>

      </div>
    );
  }
}

const AuthForm = Form.create()(Auth);

export { AuthForm };
