import * as React from 'react';
import { List, Avatar, InputNumber, Form, Button, notification } from 'antd';

import './NeedUpdate.scss';
import { IMaterialService } from '../../services/MaterialService';

import { IMaterial, IOffer } from '../../types/DTO';
import { IOfferService } from '../../services/OfferService';

interface IProps {
  MaterialService: IMaterialService
  OfferService: IOfferService
}

interface IState {
  data: IMaterial[]
  forms: { [key: string]: IOffer } | null;
}


class NeedUpdate extends React.Component<IProps, IState> {
  public state: IState = {
    data: [],
    forms: null
  };

  public componentDidMount() {
    this.props.MaterialService.getNeedUpdate()
      .then((json) => {
        const materials = json.results;
        this.setState({data: materials}, this.initForms);
      })
  };

  public render() {
    return (
      <List
        className="NeedUpdate"
        itemLayout="horizontal"
        dataSource={this.state.data}
        renderItem={this.getRenderItem()}
      />
    );
  }

  private initForms = () => {
    const { data } = this.state;
    const forms = {};
    data.map((material: IMaterial) => {
      forms[material.id] = {
        material: material.label,
        buyPrice: 0,
        sellPrice: 0
      }
    });
    this.setState({ forms });
  };

  private handleFocus = (event: React.FormEvent<HTMLInputElement>) => {
    event.currentTarget.select();
  };

  private handleSaveOffer = (id: number) => () => {
    const { OfferService, MaterialService } = this.props;
    const { forms } = this.state;
    if (!(forms && forms[id])) {
      return
    }
    const data: IOffer = {
      material: forms[id].material,
      buyPrice: forms[id].buyPrice,
      sellPrice: forms[id].sellPrice
    };
    OfferService.createOffer(data).then(() => {
      notification['info']({
        message: "Success Offer creation."
      });
      MaterialService.getNeedUpdate()
        .then((json) => {
          const materials = json.results;
          this.setState({data: materials}, this.initForms);
        })

    }).catch(() => notification['error']({
      message: "Offer creation is failed."
    }));
  };

  private handleChangeOffer = (id: number, key: string) => (value: number) => {
    this.setState((state) => {
      if (state.forms === null || state.forms[id] === null) {
        return { ...state }
      }
      return {
        forms: {
          ...state.forms,
          [id]: {
            ...state.forms[id],
            [key]: value
          }
        }
      }
    })
  };

  private getRenderItem() {
    const { forms } = this.state;

    return (item: IMaterial) => {
      if ( !forms ) {
        return <div>Empty</div>;
      }
      return (
        <List.Item>
          <List.Item.Meta
            avatar={<Avatar shape="square" src="https://l2central.info/w/images/d/d2/Item_19464.jpg" />}
            description={item.date}
            title={item.label}
          />
          <Form layout="inline">
            <Form.Item >
              <InputNumber
                onFocus={this.handleFocus}
                placeholder="Покупка"
                value={forms[item.id].buyPrice}
                onChange={this.handleChangeOffer(item.id, 'buyPrice')}
              />
            </Form.Item>
            <Form.Item >
              <InputNumber
                onFocus={this.handleFocus}
                placeholder="Продажа"
                value={forms[item.id].sellPrice}
                onChange={this.handleChangeOffer(item.id, 'sellPrice')}
              />
            </Form.Item>
            <Form.Item>
              <Button onClick={this.handleSaveOffer(item.id)}>
                Добавить
              </Button>
            </Form.Item>
          </Form>
        </List.Item>
      )
    };
  }
}

export { NeedUpdate };
