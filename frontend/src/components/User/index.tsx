import * as React from 'react';

import { Button } from 'antd';
import { IUserService, UserService as UserServiceClass } from '../../services/UserService';

interface IState {
  UserService: IUserService;
}

interface IProps {
  history: any;
  updateUserName: () => void;
}

class User extends React.Component<IProps, IState> {

  public state = {
    UserService: new UserServiceClass()
  };


  public render() {
    return (
      <Button onClick={this.handleLogout}>Выйти</Button>
    );
  }

  private handleLogout = () => {
    const { UserService } = this.state;
    const { history, updateUserName } = this.props;
    localStorage.clear();
    UserService.logout()
      .then(() => {
        history.push('/');
        updateUserName();
      })
      .catch(() => {
        console.error('Logout не работает.')
      })
  };
}

export { User };