import datetime

from django.core.validators import MinValueValidator
from django.db import models


class Material(models.Model):
    label = models.CharField(max_length=64)
    consists_of = models.ManyToManyField('Material', through='Craft')

    def __str__(self):
        return "%s" % (self.label,)

    def first_14_entries(self):
        return reversed(self.offers.all().order_by('-date')[:14])

    def last_offer_buy_price(self):
        if self.offers.exists():
            return self.offers.latest('date').buyPrice
        return 0

    def best_craft_price(self):
        consists_of = self.consistsOf
        last_buy_price = self.last_offer_buy_price()
        if consists_of.exists():
            return min(sum(map(lambda x: x.from_material.best_craft_price() * x.count, consists_of.all())), last_buy_price)
        return last_buy_price


class Craft(models.Model):
    to_material = models.ForeignKey(Material, on_delete=models.CASCADE, related_name='consistsOf')
    from_material = models.ForeignKey(Material, on_delete=models.CASCADE, related_name='craftsTo')
    count = models.PositiveIntegerField()

    def __str__(self):
        return "%s - %s" % (self.to_material.label, self.from_material.label)


class Offer(models.Model):
    material = models.ForeignKey(Material, on_delete=models.CASCADE, related_name='offers')
    date = models.DateTimeField(auto_now_add=True)
    buyPrice = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    sellPrice = models.PositiveIntegerField(validators=[MinValueValidator(1)])

    @property
    def get_days_ago(self):
        delta = (datetime.datetime.now() - self.date).days
        return delta

    @property
    def get_formatted_date(self):
        return "{} (-{} days)".format(self.date.strftime("%A %H:%M"), (datetime.datetime.now() - self.date).days)


    @property
    def marge_percent(self):
        return round(self.sellPrice / float(self.buyPrice) - 1, 2)

    @property
    def marge(self):
        return self.sellPrice - self.buyPrice

    def __str__(self):
        return "%s - %s" % (self.date, self.material.label)
