from django.db.models import Max, F, Q, OuterRef, Subquery, FloatField, \
    IntegerField
from django.db.models.functions import Cast
from rest_framework import viewsets, permissions
from datetime import datetime, timedelta

from backend.base.models import Material, Offer, ClanWarehouse
from backend.base.serializers import MaterialSerializer, MaterialListSerializer, \
    OfferSerializer, \
    ClanWarehouseListSerializer, ClanWarehouseDetailSerializer, \
    TopMaterialSerializer


class MaterialViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Material.objects.all().order_by('label')
    serializer_class = MaterialSerializer


class OfferViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Offer.objects.all().order_by('-date')
    serializer_class = OfferSerializer


class TopMaterialViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    last_offer = Offer.objects.filter(material=OuterRef('pk')).order_by('-date')
    queryset = Material.objects.annotate(
        last_buy=Subquery(last_offer.values('buyPrice')[:1]),
        last_sell=Subquery(last_offer.values('sellPrice')[:1])
    ).annotate(
        marge_percent=Cast(
            F('last_sell') * 100 / Cast(F('last_buy'), FloatField()) - 100,
            IntegerField()
        )
    )[:12]

    serializer_class = TopMaterialSerializer


class UpdateListViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    queryset = Material.objects.annotate(date=Max('offers__date')).filter(Q(date__lte=datetime.now()-timedelta(days=1))
                                                                          | Q(date__isnull=True)).order_by(F('date').asc(nulls_first=True))
    serializer_class = MaterialListSerializer
