from rest_framework.relations import SlugRelatedField

from backend.base.models import Material, Offer
from rest_framework import serializers


class OfferSerializer(serializers.ModelSerializer):
    date = serializers.CharField(read_only=True, source='get_formatted_date')
    material = SlugRelatedField(read_only=False, slug_field='label', queryset=Material.objects.all())
    margePercent = serializers.FloatField(read_only=True, source='marge_percent')
    marge = serializers.IntegerField(read_only=True)

    class Meta:
        model = Offer
        fields = ('id', 'material', 'date', 'buyPrice', 'sellPrice', 'margePercent', 'marge')


class MaterialSerializer(serializers.ModelSerializer):
    offers = OfferSerializer(many=True, read_only=True, source='first_14_entries')
    bestCraftPrice = serializers.IntegerField(min_value=1, source='best_craft_price')

    class Meta:
        model = Material
        fields = ('id', 'label', 'offers', 'bestCraftPrice')


class TopMaterialSerializer(serializers.ModelSerializer):
    margePercent = serializers.IntegerField(min_value=0, source='marge_percent')
    lastBuy = serializers.IntegerField(min_value=1, source='last_buy')
    lastSell = serializers.IntegerField(min_value=1, source='last_sell')

    class Meta:
        model = Material
        fields = ('id', 'label', 'margePercent', 'lastBuy', 'lastSell')


class MaterialListSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(format="%Y.%m.%d %H:%M:%S", required=False, read_only=True)

    class Meta:
        model = Material
        fields = ('id', 'label', 'date')
