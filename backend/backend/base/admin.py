from django.contrib import admin
from backend.base.models import Material, Craft, Offer


class CraftInline(admin.TabularInline):
    model = Craft
    fk_name = "to_material"


class MaterialAdmin(admin.ModelAdmin):
    inlines = [
        CraftInline
    ]


admin.site.register(Material, MaterialAdmin)
admin.site.register(Offer)
